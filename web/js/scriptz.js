// Activate CSS animation 1 second after page load
const addLoadedClass = () => {
	document.body.className += " loaded";
}

// Turn the whole flyer into a clickable link
const makeFlyerClickable = () => {
	document.querySelector("#flyer").addEventListener("click", () => {
		window.location.href = "https://www.residentadvisor.net/events/1373450";
	});
}

setTimeout(makeFlyerClickable, 500);
setTimeout(addLoadedClass, 1000);
