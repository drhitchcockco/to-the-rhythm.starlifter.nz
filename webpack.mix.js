let mix = require('laravel-mix');
require('mix-tailwindcss');
require('laravel-mix-purgecss');

mix.sass('./src/sass/stylez.scss', './web/css/stylez.css')
   .tailwind()
   .purgeCss({
      extensions: ['html'],
      folders: ['src'],
      content: [], // content
      css: [], // css
      whitelistPatterns: [/loaded$/],
   })
   .copy('./src/*.html', './web/')
   .copyDirectory('src/imgz', 'web/imgz')
   .copyDirectory('src/js', 'web/js')
   .copyDirectory('src/fonts', 'web/fonts')
   .options({
      processCssUrls: false,
      purifyCss: false,
      postCss: [
         require('autoprefixer'), 
      ],
      clearConsole: true,
      publicPath: 'web'
   })
